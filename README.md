## Objective

This GitLab CI/CD pipeline is designed to create a scratch org for build-testing and deploy to production. The pipeline also installs necessary dependencies, sets up Salesforce DX environment variables, and decrypts the server key.\

This template is imported  from [Apex Hours article](https://www.apexhours.com/ci-cd-pipeline-using-gitlab-for-salesforce/).

## How to use it

1. Set the necessary GitLab variables listed below as described in [the article](https://www.apexhours.com/ci-cd-pipeline-using-gitlab-for-salesforce/)

## Variables

| Name | Description |
| ---- | ----------- |
| `SERVER_KEY_PASSWORD` | Password for the encrypted server key |
| `SF_CONSUMER_KEY_PROD` | Consumer key for the Salesforce Production environment |
| `SF_USERNAME_PROD` | Username for the Salesforce Production environment |

## Pipeline Stages

### `build-testing`
- Creates a scratch org for build-testing
- Authenticates to the Dev Hub using the server key
- Pushes source code to scratch org
- Deletes scratch org after testing is complete

### `prod-deploy`
- Deploys source code to the production environment
- Authenticates to the Dev Hub using the server key
- Requires manual intervention to execute